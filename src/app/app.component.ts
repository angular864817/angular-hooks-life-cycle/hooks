import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hooks';

  private number = 4445454



  get counter(){
    return  this.number
    }

  set counter(value){
   this.number=value
  }

  increment(){
    this.counter++;
  }
  decrement(){
    this.counter--;
  }

  name:string = "sahil"

  // if we chnage value of name like this direct then it will stop at ngonchange and then stop on docheck because it was pass by reference
  // updateval(){
  //   this.name="king "
  // }



  // if you are  change value of this name by create obj and then change value then it was stop at dirct on docheck because we create new obj  so it was pass by value.
  user: any={
    name:"sahil"
  }
  updateval(){
    this.user.name="king"
  }

  // for only destroy component

  isbool:boolean= true;
  visibale()
  {
    this.isbool =! this.isbool
    console.log(this.isbool)
  }

}
