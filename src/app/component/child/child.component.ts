import { Component, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent {

  // @Input() MyNewNumber=0;

  @Input() newname:any ;





  private mynumber!: Number;

  @Input()
  set MyNewNumber(number:Number){
    this.mynumber=number
  }

  get MyNewNumber(){
    return this.mynumber
  }

  ngOnChanges(changes: SimpleChanges): void {
    // const val = changes['MyNewNumber']
    // console.log(val.currentValue)

    const newnumberchange=  changes['MyNewNumber']
    debugger
    console.log(newnumberchange.currentValue,"curr")
    console.log(newnumberchange.previousValue,"prev ")
  }

  ngOnInit(): void {
    console.log("i am ngoninit  ")

  }

  ngDoCheck(): void {
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    //Add 'implements DoCheck' to the class.
    debugger

  }

  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    console.log("sjidffedf")
  }

  ngAfterContentChecked(): void {
    //Called after every check of the component's or directive's content.
    //Add 'implements AfterContentChecked' to the class.
    console.log("adxdsd")
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    console.log("cvfb h")
  }
  ngAfterViewChecked(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
    console.log("wdhbhbcx")
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    console.log("component is destroyes")
  }
}
